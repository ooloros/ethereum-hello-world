require('dotenv').config();
const {API_URL, PUBLIC_KEY, PRIVATE_KEY} = process.env;

const {createAlchemyWeb3} = require("@alch/alchemy-web3");
const web3 = createAlchemyWeb3(API_URL);

// For Hardhat
const contract = require("../artifacts/contracts/Greeter.sol/Greeter.json");

const contractAddress = "0x18d86b0f3bb89C7E347232b8F827c87Ab42A7E93";
const greeterContract = new web3.eth.Contract(contract.abi, contractAddress);

console.log(JSON.stringify(contract.abi));

async function main() {
    const message = await greeterContract.methods.greet().call();
    console.log("The message is: " + message);
}

main();