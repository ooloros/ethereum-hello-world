require('dotenv').config();
const {API_URL, PUBLIC_KEY, PRIVATE_KEY} = process.env;

const {createAlchemyWeb3} = require("@alch/alchemy-web3");
const web3 = createAlchemyWeb3(API_URL);

// For Hardhat
const contract = require("../artifacts/contracts/Greeter.sol/Greeter.json");

const contractAddress = "0x18d86b0f3bb89C7E347232b8F827c87Ab42A7E93";
const greeterContract = new web3.eth.Contract(contract.abi, contractAddress);

console.log(JSON.stringify(contract.abi));

async function updateMessage(newMessage) {
    const nonce = await web3.eth.getTransactionCount(PUBLIC_KEY, 'latest'); // get latest nonce
    const gasEstimate = await greeterContract.methods.setGreeting(newMessage).estimateGas(); // estimate gas
    const tx = {
        'from': PUBLIC_KEY,
        'to': contractAddress,
        'nonce': nonce,
        'gas': gasEstimate,
        'maxFeePerGas': 1000000108,
        'data': greeterContract.methods.setGreeting(newMessage).encodeABI()
    };
    // Sign the transaction
    const signPromise = web3.eth.accounts.signTransaction(tx, PRIVATE_KEY);
    signPromise.then((signedTx) => {
        web3.eth.sendSignedTransaction(signedTx.rawTransaction, function(err, hash) {
            if (!err) {
                console.log("The hash of your transaction is: ", hash, "\n Check Alchemy's Mempool to view the status of your transaction!");
            } else {
                console.log("Something went wrong when submitting your transaction:", err)
            }
        });
    }).catch((err) => {
        console.log("Promise failed:", err);
    });
}

async function main() {
    await updateMessage("Hello Drupe!");
}

main();